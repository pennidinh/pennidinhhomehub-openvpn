FROM debian:stretch

RUN apt-get update && apt-get install -y openvpn expect ruby2.3 ruby2.3-dev iptables

RUN gem install redis -v 4.4.0
RUN cp -RT /usr/share/easy-rsa /etc/openvpn 
COPY /vars /etc/openvpn/vars
RUN cd /etc/openvpn && chmod 777 openssl-1.0.0.cnf
RUN cd /etc/openvpn && sed -i "s/default_crl_days\s*=.*/default_crl_days = 36500/g" openssl-1.0.0.cnf
RUN cd /etc/openvpn && sed -i "s/default_days\s*=.*/default_days = 36500/g" openssl-1.0.0.cnf

COPY daemon.sh /
COPY create-client-key-with-pass.sh /etc/openvpn/
COPY update-clients.rb /etc/openvpn/
COPY openvpn-server.conf /
COPY openvpn-client.ovpn.erb /
COPY poll-for-updates-daemon.rb  /etc/openvpn/

CMD ./daemon.sh
