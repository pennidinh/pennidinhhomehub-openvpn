#!/bin/bash

mkdir -p /dev/net
mknod /dev/net/tun c 10 200
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

cd /etc/openvpn
source ./vars

cd /etc/openvpn
source ./vars

mkdir /etc/openvpn/keys
cp "$KEYS_PATH/index.txt" "/etc/openvpn/keys/index.txt" 
cp "$KEYS_PATH/index.txt.attr" "/etc/openvpn/keys/index.txt.attr" 
cp "$KEYS_PATH/serial" "/etc/openvpn/keys/serial" 
cp "$KEYS_PATH/crl.pem" "/etc/openvpn/keys/crl.pem"

cp "$KEYS_PATH/ca.key" "/etc/openvpn/keys/ca.key" 
cp "$KEYS_PATH/ca.crt" "/etc/openvpn/keys/ca.crt" 
if [ ! -f "/etc/openvpn/keys/ca.key" ]; then
  ./clean-all
  ./pkitool --initca
  cp "/etc/openvpn/keys/ca.key" "$KEYS_PATH/ca.key"
  cp "/etc/openvpn/keys/ca.crt" "$KEYS_PATH/ca.crt"
  cp "/etc/openvpn/keys/index.txt" "$KEYS_PATH/index.txt" 
  cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
  cp "/etc/openvpn/keys/serial" "$KEYS_PATH/serial" 
fi
echo done with ca

cp "$KEYS_PATH/server.key" "/etc/openvpn/keys/server.key" 
cp "$KEYS_PATH/server.crt" "/etc/openvpn/keys/server.crt" 
if [ ! -f "/etc/openvpn/keys/server.key" ]; then
  ./pkitool --server server
  cp "/etc/openvpn/keys/server.key" "$KEYS_PATH/server.key"
  cp "/etc/openvpn/keys/server.crt" "$KEYS_PATH/server.crt"
  cp "/etc/openvpn/keys/index.txt" "$KEYS_PATH/index.txt" 
  cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
  cp "/etc/openvpn/keys/serial" "$KEYS_PATH/serial"
fi
echo done with server

cp "$KEYS_PATH/dh2048.pem" "/etc/openvpn/keys/dh2048.pem" 
if [ ! -f "/etc/openvpn/keys/dh2048.pem" ]; then
  ./build-dh
  cp "/etc/openvpn/keys/dh2048.pem" "$KEYS_PATH/dh2048.pem"
fi
echo done with diffie-hellman

ruby update-clients.rb 
cp "/etc/openvpn/keys/index.txt" "$KEYS_PATH/index.txt" 
cp "/etc/openvpn/keys/crl.pem" "$KEYS_PATH/crl.pem" 
cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
cp "/etc/openvpn/keys/serial" "$KEYS_PATH/serial"

echo done with clients

cd /etc/openvpn
if [ ! -s /etc/openvpn/keys/crl.pem ]; then
  ./create-client-key-with-pass.sh for-creating-crl-pem dummy-password
  ./revoke-full for-creating-crl-pem
  rm /etc/openvpn/keys/for-creating-crl-pem.crt
  rm /etc/openvpn/keys/for-creating-crl-pem.key
  cp "/etc/openvpn/keys/crl.pem" "$KEYS_PATH/crl.pem" 
fi
openvpn /openvpn-server.conf &

while true; do
  ruby /etc/openvpn/poll-for-updates-daemon.rb 

  ruby update-clients.rb 
  cp "/etc/openvpn/keys/index.txt" "$KEYS_PATH/index.txt" 
  cp "/etc/openvpn/keys/crl.pem" "$KEYS_PATH/crl.pem" 
  cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
  cp "/etc/openvpn/keys/index.txt.attr" "$KEYS_PATH/index.txt.attr" 
  cp "/etc/openvpn/keys/serial" "$KEYS_PATH/serial"
done
