require 'redis'

redis = Redis.new(host: "redis")

NEW_CLIENTS = 'new-clients'
REMOVE_CLIENTS = 'remove-clients'

while true do
  if redis.llen(NEW_CLIENTS) > 0 || redis.llen(REMOVE_CLIENTS) > 0
    exit 0
  else
    sleep ENV['SLEEP_BETWEEN_POLLS_IN_SECS'].to_i
  end
end
