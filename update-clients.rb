require 'redis'
require 'json'
require 'fileutils'

redis = Redis.new(host: "redis")

NEW_CLIENTS = 'new-clients'
EXISTING_CLIENTS = 'existing-clients'
REMOVE_CLIENTS = 'remove-clients'
KEYS_CREATE_PATH = '/etc/openvpn/keys/'
KEYS_PATH = ENV['KEYS_PATH']
VPN_HTT_DIR_PATH = '/openvpn-http-dir/'

while redis.llen(NEW_CLIENTS) > 0 do
  newClientStr = redis.lindex(NEW_CLIENTS, 0)
  puts "New client to add #{newClientStr}"
  newClient = JSON.parse(newClientStr)

  if File.file?(KEYS_PATH + newClient['name'] + '.key') 
    redis.lpop(NEW_CLIENTS)
    redis.rpush(EXISTING_CLIENTS, newClient['name'])
    puts "Key with name #{newClient['name']}.key already exists!"
    next
  end

  puts `./create-client-key-with-pass.sh #{newClient['name']} #{newClient['password']} && wait` 
  FileUtils.mv KEYS_CREATE_PATH + newClient['name'] + '.key', KEYS_PATH + newClient['name'] + '.key'
  FileUtils.mv KEYS_CREATE_PATH + newClient['name'] + '.crt', KEYS_PATH + newClient['name'] + '.crt'
  puts "Created client crt and key"

  ENV['NAME'] = newClient['name']

  externalSubdomain = redis.get('external-subdomain');
  while externalSubdomain.nil? do
    puts 'external subdomain not available in redis. waiting 5 seconds...'
    sleep 5

    externalSubdomain = redis.get('external-subdomain');
  end
  ENV['EXTERNAL_DOMAIN_NAME'] = externalSubdomain

  puts `erb /openvpn-client.ovpn.erb > #{VPN_HTT_DIR_PATH + newClient['name'] + '.ovpn'}`
  puts "Created client .ovpn file"

  redis.rpush(EXISTING_CLIENTS, newClient['name'])

  redis.lpop(NEW_CLIENTS)
  puts "Done"
  redis.save
end

while redis.llen(REMOVE_CLIENTS) > 0 do
  removeClient = redis.lpop(REMOVE_CLIENTS)
  redis.lrem(EXISTING_CLIENTS, 0, removeClient)
  puts "Removing #{removeClient}"

  FileUtils.cp KEYS_PATH + removeClient + '.key', KEYS_CREATE_PATH + removeClient + '.key'
  FileUtils.cp KEYS_PATH + removeClient + '.crt', KEYS_CREATE_PATH + removeClient + '.crt'
  puts `cd /etc/openvpn && ./revoke-full #{removeClient}`
  FileUtils.rm KEYS_PATH + removeClient + '.key'
  FileUtils.rm KEYS_CREATE_PATH + removeClient + '.key'
  FileUtils.rm KEYS_PATH + removeClient + '.crt'
  FileUtils.rm KEYS_CREATE_PATH + removeClient + '.crt'
  FileUtils.rm VPN_HTT_DIR_PATH + removeClient + '.ovpn'
  redis.save
  puts "Done"
end
